/*global chrome*/
import React, { useContext, useEffect } from "react";
import Disclaimer from "../components/Disclaimer";
import { AppContext } from "../context/AppContextProvider";
import { getFinalPrice, getToken } from "../hooks/helpers.js";
import Server from "../config/server.json";
import "../config/i18n.js";
import i18n from "../config/i18n";
import { useTranslation } from "react-i18next";

export default function MyLists() {
  const { t } = useTranslation();

  const [state, dispatch] = useContext(AppContext);

  const domain =
    i18n.language === "it"
      ? `https:www.bancomail.it`
      : `https:www.bancomail.com/${i18n.language}`;

  const removeList = (id) => {
    dispatch({
      type: "REMOVE_LIST",
      payload: id,
    });

    const getLists = state.lists.filter((item) => item.pi !== id);
    //prepare delete pack on server
    async function deletePackServerSide() {
      fetch(`${Server["BASE_URl"]}/extension/delete-pack/${id}`, {
        method: "DELETE",
        cache: "no-cache",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          token: await getToken(process.env.NODE_ENV),
          pack_id: id,
        }),
      })
        .then((response) => {
          if (response.ok) {
            //remove from chrome storage
            try {
              chrome.storage.local.set(
                { bm_my_lists: JSON.stringify(getLists) },
                () => {
                  if (getLists.length === 0) {
                    dispatch({
                      type: "REMOVE_LIST",
                      payload: id,
                    });
                  }
                }
              );
            } catch (e) {
              console.log(e.message + " ...update local storage");
              localStorage.setItem("bm_my_lists", JSON.stringify(getLists));
              if (getLists.length === 0) {
                dispatch({
                  type: "REMOVE_LIST",
                  payload: id,
                });
              }
            }
          }
        })
        .catch((err) => console.log(err));
      return;
    }
    //execute delete pack
    deletePackServerSide();
  };

  const toggleSync = (id, status, discount) => {
    status = status === false ? true : false;

    if (discount !== "" && discount >= 50 && status === true) {
      alert(t("La lista ha raggiunto il limite massimo di sconto!"));
      return;
    }

    const getLists = state.lists;

    getLists.forEach((item) => {
      if (item.pi === id) {
        item.updates = status;
        dispatch({
          type: "UPDATE_LIST",
          payload: { id: id, status: status },
        });
        //prepare update pack on server
        async function updatePackServerSide() {
          const data = {
            token: await getToken(process.env.NODE_ENV),
            pack_id: id,
          };
          if (status) {
            data["pack_monitor_status"] = 1;
          }
          fetch(`${Server["BASE_URl"]}/extension/update-pack`, {
            method: "PATCH",
            cache: "no-cache",
            headers: {
              "Content-Type": "application/json; charset=UTF-8",
            },
            body: JSON.stringify(data),
          })
            .then((response) => {
              if (response.ok) {
                console.log(response);
                //update pack in chrome storage
                try {
                  chrome.storage.local.set(
                    { bm_my_lists: JSON.stringify(getLists) },
                    () => console.log("Pack lists was updated successfully!")
                  );
                } catch (e) {
                  //console.log(e.message + ' ...update local storage');
                  localStorage.setItem("bm_my_lists", JSON.stringify(getLists));
                }
              }
            })
            .catch((err) => console.log(err));
          return;
        }
        //execute update pack
        updatePackServerSide();
      }
    });
  };

  const buySingleItem = (id) =>
    `${domain}/${t("liste-email")}/${t("carrello")}?packs=${id}`;

  const buyAll = () => {
    const ids = [];
    state.lists.forEach((item) => ids.push(item.pi));
    return `${domain}/${t("liste-email")}/${t("carrello")}?packs=${ids.join(
      ","
    )}`;
  };

  const updateDiscount = (id, discount) => {
    dispatch({
      type: "UPDATE_LIST",
      payload: { id: id, discount: discount },
    });

    try {
      chrome.storage.local.set(
        { bm_my_lists: JSON.stringify(state.lists) },
        function () {
          return;
        }
      );
    } catch (e) {
      //console.log(e.message + ' ...update local storage');
      localStorage.setItem("bm_my_lists", JSON.stringify(state.lists));
    }
  };

  useEffect(() => {
    try {
      //delete all previous badge
      chrome.action.setBadgeText({ text: "" }, () => false);
    } catch (e) {
      console.log(e.message);
    }
    async function getPack(item) {
      let pack = false;
      try {
        let getPack = await fetch(
          `${
            Server["BASE_URl"]
          }/extension/get-pack-info?pack_id=${encodeURIComponent(
            item.pi
          )}&lang=${encodeURIComponent(i18n.language)}`
        );
        let response = await getPack.json(process.env.NODE_ENV);
        pack = response.data[0];
      } catch (e) {
        console.log(e.message);
      }
      return pack;
    }

    state.lists.forEach(async (item) => {
      if (item.updates === false) {
        let getItemDiscount =
          typeof item.updatesInfo !== "undefined" &&
          typeof item.updatesInfo.discount !== "undefined"
            ? item.updatesInfo.discount
            : item.d;
        let pack = await getPack(item).catch((error) =>
          console.log(error.message)
        );
        if (pack && getItemDiscount !== pack.d) {
          updateDiscount(pack.pi, pack.d);
        }
      }
    });
  }, [state.lists]);

  return (
    <React.Fragment>
      <div className="listContainer">
        <ul className="lists">
          {state.lists.map((item, i) => (
            <li key={i} className="flex flex-middle">
              <div>
                <p>
                  <strong>{item.pn}</strong>
                </p>
                <p>
                  <span className={`flag-${item.iso.toLowerCase()}`}></span>
                  {item.n} - {t(item.re)}
                </p>
                <p>
                  {t("Anagrafiche")}: {item.i}
                </p>
              </div>
              <div className="list-price-holder">
                <span
                  className={
                    (item.d !== "" && item.d !== null && item.d > 0) ||
                    (typeof item.updatesInfo !== "undefined" &&
                      typeof item.updatesInfo.discount !== "undefined" &&
                      item.updatesInfo.discount !== null &&
                      item.updatesInfo.discount > 0)
                      ? "linetrough"
                      : ""
                  }
                >
                  {new Intl.NumberFormat("it-IT", {
                    style: "currency",
                    currency: "EUR",
                  }).format(item.p)}
                </span>
                {((item.d !== "" && item.d !== null && item.d > 0) ||
                  (typeof item.updatesInfo !== "undefined" &&
                    typeof item.updatesInfo.discount !== "undefined" &&
                    item.updatesInfo.discount !== null &&
                    item.updatesInfo.discount > 0)) && (
                  <span>
                    {typeof item.updatesInfo !== "undefined" &&
                    typeof item.updatesInfo.discount !== "undefined" &&
                    item.updatesInfo.discount !== null &&
                    item.updatesInfo.discount > 0
                      ? getFinalPrice(item.updatesInfo.discount, item.p)
                      : getFinalPrice(item.d, item.p)}
                  </span>
                )}
              </div>
              <div className="sync-icon-holder tooltip">
                {typeof item.updatesInfo === "undefined" ? (
                  <React.Fragment>
                    <svg
                      className="icn"
                      onClick={() => toggleSync(item.pi, item.updates, item.d)}
                    >
                      {!item.updates ? (
                        <use href="#no-sync-icon"></use>
                      ) : (
                        <use href="#sync-icon"></use>
                      )}
                    </svg>

                    <span className="tooltiptext">
                      {item.updates === false
                        ? t("Avviare il monitoraggio")
                        : t("Ferma il monitoraggio")}
                    </span>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <svg className="icn shakeItbabe">
                      {typeof item.updatesInfo !== "undefined" &&
                        typeof item.updatesInfo.discount !== "undefined" &&
                        item.updatesInfo.discount !== null && (
                          <use href="#bell-icon"></use>
                        )}
                    </svg>
                    {typeof item.updatesInfo !== "undefined" &&
                    typeof item.updatesInfo.discount !== "undefined" &&
                    item.updatesInfo.discount !== null ? (
                      <span className="tooltiptext big">
                        {t("Sconto")} : {item.updatesInfo.discount}%
                      </span>
                    ) : (
                      <svg
                        className="icn"
                        onClick={() => toggleSync(item.pi, false, item.d)}
                      >
                        {item.updates === false ? (
                          <use href="#no-sync-icon"></use>
                        ) : (
                          <use href="#sync-icon"></use>
                        )}
                      </svg>
                    )}
                  </React.Fragment>
                )}
              </div>
              <div>
                <div className="tooltip">
                  <svg
                    className="icn"
                    onClick={() => removeList(item.pi)}
                    title={t("Rimuovi")}
                  >
                    <use href="#trash-icon"></use>
                  </svg>
                  <span className="tooltiptext">{t("Rimuovi")}</span>
                </div>
                <a
                  href={buySingleItem(item.pi)}
                  className="btn"
                  target="_blank"
                  rel="noreferrer"
                >
                  {t("Acquista")}
                </a>
              </div>
            </li>
          ))}
        </ul>
        {state.lists.length > 0 && <Disclaimer translate={t} />}
        {state.lists.length > 1 && (
          <a
            className="btn"
            title={t("Acquista su Bancomail")}
            target="_blank"
            href={buyAll()}
            rel="noreferrer"
          >
            {t("Acquista tutte le liste")}
          </a>
        )}
        {state.lists.length === 0 && (
          <p className="prompt-msg">
            <strong>{t("Non hai salvato nessuna lista!")}</strong>
          </p>
        )}
      </div>
    </React.Fragment>
  );
}
