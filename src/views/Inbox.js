/*global chrome*/
import React, { useContext, useState, useEffect } from "react";
import { AppContext } from "../context/AppContextProvider";
import UnsafeComponent from "../components/UnsafeComponent";
import {
  getToken,
  htmlDecode,
  removeSlashes,
  formatDate,
} from "../hooks/helpers.js";
import Server from "../config/server.json";
import "../config/i18n.js";
import i18n from "../config/i18n";
import { useTranslation } from "react-i18next";

export default function Inbox() {
  const { t } = useTranslation();
  const [state, dispatch] = useContext(AppContext);
  const [notifications, setNotifications] = useState(false);

  const deleteNotifications = (receivedDate) => {
    try {
      chrome.storage.local.get(["bm_notifications"], function (result) {
        if (result.bm_notifications) {
          const notifications = JSON.parse(result.bm_notifications);
          notifications.forEach((item) => {
            if (item.received_date === receivedDate) {
              delete item["message_title"];
              delete item["message_descr"];
              delete item["btn_title"];
              delete item["call_to_action"];
              delete item["note"];
            }
          });
          chrome.storage.local.set(
            { bm_notifications: JSON.stringify(notifications) },
            function () {
              return;
            }
          );
          setNotifications(notifications);
          dispatch({
            type: "UPDATE_INBOX",
            payload: notifications.filter(
              (item) =>
                item.message_title !== undefined &&
                item.message_descr !== undefined
            ).length,
          });
        }
      });
    } catch (e) {
      const notifications = JSON.parse(
        localStorage.getItem("bm_notifications")
      );
      notifications.forEach((item) => {
        if (item.received_date === receivedDate) {
          delete item["message_title"];
          delete item["message_descr"];
          delete item["btn_title"];
          delete item["call_to_action"];
          delete item["note"];
        }
      });

      localStorage.setItem("bm_notifications", JSON.stringify(notifications));
      setNotifications(notifications);
      dispatch({
        type: "UPDATE_INBOX",
        payload: notifications.filter(
          (item) =>
            item.message_title !== undefined && item.message_descr !== undefined
        ).length,
      });
    }
  };

  useEffect(() => {
    if (notifications === false) {
      // in development make a call to server in order to get new notifications
      if (process.env.NODE_ENV === "development") {
        const oldNotifications =
          localStorage.getItem("bm_notifications") !== null
            ? JSON.parse(localStorage.getItem("bm_notifications")).filter(
                (item) =>
                  item.message_title !== undefined &&
                  item.message_descr !== undefined
              )
            : [];
        async function fetchData(token, language, bulk) {
          const getData = await fetch(
            `${Server["BASE_URl"]}/extension/get-fresh-notifications?token=${token}&lang=${language}&bulk=${bulk}`
          );
          const response = await getData.json();
          if (response.ok) {
            const newNotifications = [];

            response.data.forEach((item) => {
              let status = true;
              oldNotifications.forEach((element) => {
                if (
                  formatDate(element.start_date) ===
                    formatDate(item.start_date) &&
                  formatDate(element.end_date) === formatDate(item.end_date) &&
                  element.bulk === item.bulk &&
                  element.lang === item.lang
                ) {
                  status = false;
                }
                if (
                  item.user_token !== undefined &&
                  item.user_token !== "" &&
                  item.user_token !== null &&
                  item.user_token === element.user_token
                ) {
                  status = false;
                }
              });
              if (status) {
                item["received_date"] = Date.now();
                newNotifications.push(item);
              }
            });
            if (newNotifications.length > 0) {
              newNotifications.forEach((item) =>
                oldNotifications.unshift(item)
              );
              localStorage.setItem(
                "bm_notifications",
                JSON.stringify(oldNotifications)
              );
              dispatch({
                type: "UPDATE_INBOX",
                payload: oldNotifications.length,
              });
            }
          }
          setNotifications(oldNotifications);
        }
        getToken(process.env.NODE_ENV).then((token) => {
          const language = i18n.language;
          const lists =
            localStorage.getItem("bm_notifications") !== null
              ? JSON.parse(localStorage.getItem("bm_notifications"))
              : [];
          const bulk = lists.length === 0 ? 1 : 2;
          fetchData(token, language, bulk);
        });
      } else {
        //in production read from chrome storage
        //chrome storage is updated in ../../public/js/background.js
        try {
          chrome.storage.local.get(["bm_notifications"], function (result) {
            if (result.bm_notifications) {
              const notifications = JSON.parse(result.bm_notifications).filter(
                (item) =>
                  item.message_title !== undefined &&
                  item.message_descr !== undefined
              );
              setNotifications(notifications);
            }
          });
        } catch (e) {
          console.log(e.message);
        }
      }
    }
  }, []);

  return (
    <React.Fragment>
      <div className="listContainer">
        {notifications ? (
          <ul className="lists">
            {notifications.map((item, i) => {
              const d = new Date(item.received_date);
              if (item.message_title && item.message_descr) {
                return (
                  <li key={i}>
                    <p>
                      <span className="f-right">
                        <svg
                          className="icn"
                          onClick={(e) => {
                            deleteNotifications(item.received_date);
                          }}
                        >
                          <use href="#close-icon"></use>
                        </svg>
                      </span>
                      <span>{`${d.getFullYear()}-${d.getMonth()}-${d.getDay()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`}</span>
                    </p>
                    <UnsafeComponent
                      html={`<strong class="flex-item">${removeSlashes(
                        htmlDecode(item.message_title)
                      )}</strong>`}
                    />
                    <UnsafeComponent
                      html={removeSlashes(htmlDecode(item.message_descr))}
                    />

                    <p className="mtop10">
                      <a
                        href={removeSlashes(item.call_to_action)}
                        className="btn"
                        target="_blank"
                        rel="noreferrer"
                      >
                        {removeSlashes(item.btn_title)}
                      </a>
                    </p>
                  </li>
                );
              }
            })}
            {notifications.length === 0 && (
              <p className="prompt-msg">
                <strong>{t("Non hai nessuna notifica!")}</strong>
              </p>
            )}
          </ul>
        ) : (
          <p className="prompt-msg">
            <strong>{t("Non hai nessuna notifica!")}</strong>
          </p>
        )}
      </div>
    </React.Fragment>
  );
}
