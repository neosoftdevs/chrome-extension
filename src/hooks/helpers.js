/*global chrome*/

export const getFinalPrice = (discount, price) => {
  if (discount === null || discount === 0) {
    return false;
  }
  const getValue = (price * discount) / 100;
  const finalPrice = price - getValue;
  return new Intl.NumberFormat("it-IT", {
    style: "currency",
    currency: "EUR",
  }).format(finalPrice);
};

export const getLanguage = () => {
  const getInfo = () =>
    new Promise((resolve, reject) => {
      try {
        chrome.storage.local.get(["bm_lang"], function (result) {
          if (result.bm_lang) {
            resolve(result.bm_lang);
          } else {
            reject("Eror retriving bm_lang");
          }
        });
      } catch (e) {
        if (localStorage.getItem("bm_lang") !== null) {
          resolve(localStorage.getItem("bm_lang"));
        } else {
          resolve(navigator.language.split("-")[0]);
        }
      }
    });

  const extractInfo = async () => {
    const data = await getInfo();
    return data;
  };

  return extractInfo();
};

export const showHiddenBtns = () => {
  try {
    document.querySelectorAll(".lists li button").forEach((element) => {
      element.classList.remove("hidden");
      element.nextSibling.classList.add("hidden");
    });
  } catch (e) {
    console.log(e.message);
  }
};

export const getToken = (environment = false) =>
  new Promise((resolve, reject) => {
    if (environment && environment === "development") {
      if (localStorage.getItem("userId") !== null) {
        const userInfo = JSON.parse(localStorage.getItem("userId"));
        resolve(userInfo.token);
      }
    } else {
      chrome.storage.local.get(["bm_token"], function (result) {
        if (result.bm_token) {
          resolve(result.bm_token);
        } else {
          reject(false);
        }
      });
    }
  });

export function removeSlashes(string) {
  if (typeof string !== "undefined" && string.length > 0) {
    string = string
      .replace(/\\"/g, '"')
      .replace(/\\'/g, "'")
      .replace(/\\r/g, "\r")
      .replace(/\\f/g, "\f")
      .replace(/\\n/g, "\n")
      .replace(/\\t/g, "\t")
      .replace(/\\b/g, "\u0008")
      .replace(/\\\\/g, "\\");
    return string.trim();
  }
}

export function htmlDecode(txt) {
  if (txt === "" || typeof txt === "undefined" || txt === null) {
    return;
  }
  let replaceChars = {
    "&lt;": "<",
    "&gt;": ">",
    "&apos;": "'",
    "&quot;": '"',
    "&amp;": "&",
  };
  let decodedTxt = txt.replace(
    /&lt;|&gt;|&apos;|&quot;|&amp;/g,
    function (match) {
      return replaceChars[match];
    }
  );
  return decodedTxt;
}

export function formatDate(input, output) {
  if (input === "" || typeof input === "undefined" || input === null) {
    return;
  }
  const data = input.replace("T", " ").replace(".000Z", "").replace(":00", "");
  const getData = data.split(" ");
  if (output === "date") {
    return getData[0];
  } else if (output === "time") {
    return getData[1];
  } else {
    return data;
  }
}
