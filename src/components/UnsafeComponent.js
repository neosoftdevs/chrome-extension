import React from "react";

function UnsafeComponent({ html }) {
  return <p dangerouslySetInnerHTML={{ __html: html }} />;
}

export default UnsafeComponent;
