/*global chrome*/
import React, { useState, useContext, useEffect } from "react";
import { AppContext } from "../context/AppContextProvider";

const Header = (props) => {
  const [state, dispatch] = useContext(AppContext);
  const [inbox, setInbox] = useState(false);

  const switchView = (target, value) => {
    target.preventDefault();
    props.setPage(value);
  };

  useEffect(() => {
    if (state.lists.length === 0) {
      //update state

      const getStorage = () =>
        new Promise((resolve, reject) => {
          try {
            chrome.storage.local.get(["bm_my_lists"], function (result) {
              if (result.bm_my_lists) {
                resolve(result.bm_my_lists);
              } else {
                reject("Error retriving bm_my_lists from chrome.storage");
              }
            });
          } catch (e) {
            if (localStorage.getItem("bm_my_lists") !== null) {
              resolve(localStorage.getItem("bm_my_lists"));
            }
          }
        });
      const extractFromStorage = async () => {
        const data = await getStorage();
        if (data !== null) {
          const storedLists = JSON.parse(data);
          storedLists.forEach((item) => {
            dispatch({
              type: "ADD_LIST",
              payload: item,
            });
          });
        }
      };

      extractFromStorage();
    }
    if (inbox === false) {
      try {
        // in production mode read from chrome storage
        chrome.storage.local.get(["bm_notifications"], function (result) {
          if (result.bm_notifications) {
            let index = 0;
            const items = JSON.parse(result.bm_notifications);
            items.forEach((item) => {
              if (item.message_title && item.message_descr) {
                index++;
              }
            });
            setInbox(index);
            dispatch({
              type: "UPDATE_INBOX",
              payload: index,
            });
          }
        });
      } catch (e) {
        //in development mode read from local storage
        if (localStorage.getItem("bm_notifications") !== null) {
          let index = 0;
          const items = JSON.parse(localStorage.getItem("bm_notifications"));
          items.forEach((item) => {
            if (item.message_title && item.message_descr) {
              index++;
            }
          });
          setInbox(index);
          dispatch({
            type: "UPDATE_INBOX",
            payload: index,
          });
        } else {
          //define bm_notifications
          localStorage.setItem("bm_notifications", JSON.stringify([]));
          setInbox("0");
        }
      }
    }
  }, [state, dispatch, inbox]);

  return (
    <header>
      <nav>
        <ul className="flex flex-middle">
          <li className="flex-item">
            <img
              src="https://www.bancomail.it/images/bancomail-rm-logo.png"
              className="logo"
              alt=""
            />
          </li>
          <li className="flex flex-middle flex-item auto">
            <a href="/" onClick={(e) => switchView(e, "search")}>
              <svg className="icn">
                <use href="#search-icon"></use>
              </svg>
              <span>{props.translate("Cerca")}</span>
            </a>
            <a href="/" onClick={(e) => switchView(e, "mylists")}>
              {state.lists.length > 0 && <small>{state.lists.length}</small>}
              <svg className="icn">
                <use href="#folder-open-icon"></use>
              </svg>
              <span>{props.translate("Le mie liste")}</span>
            </a>
            <a
              href="/"
              onClick={(e) => {
                switchView(e, "inbox");
                try {
                  //delete all previous badge
                  chrome.action.setBadgeText({ text: "" }, () => false);
                } catch (e) {
                  console.log(e.message);
                }
              }}
            >
              <small>{state.inbox}</small>
              <svg className="icn">
                <use href="#bell-icon-white"></use>
              </svg>
            </a>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
