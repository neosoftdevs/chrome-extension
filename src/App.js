import React, { useState, useEffect, Suspense } from "react";
import Preloader from "./components/Preloader";
import { AppContextProvider } from "./context/AppContextProvider";
import { SearchContextProvider } from "./context/SearchContextProvider";
import { getLanguage } from "./hooks/helpers";
import i18Languages from "./config/i18Languages";
import i18n from "./config/i18n";
import Header from "./components/Header";
import "./App.css";
import "./config/i18n.js";
import Server from "./config/server.json";
import { useTranslation } from "react-i18next";

const App = () => {
  const { t } = useTranslation();
  const [list, setList] = useState([]);

  //use state to manage views
  const [page, setPage] = useState("search");

  //load views
  const Search = React.lazy(() => import(`./views/Search.js`));
  const MyLists = React.lazy(() => import(`./views/MyLists.js`));
  const Inbox = React.lazy(() => import(`./views/Inbox.js`));

  const toggleView = (view) => setPage(view);

  useEffect(() => {
    if (list !== undefined && list.length === 0) {
      async function fetchData() {
        try {
          const lng = await getLanguage();
          const language = i18Languages.indexOf(lng) !== -1 ? lng : "en";
          i18n.changeLanguage(language);
          const response = await fetch(
            `${Server["BASE_URl"]}/extension/get-packs?lang=${language}`
          );
          const userData = await response.json();
          setList(userData.packs);
        } catch (e) {
          console.log(e.message);
        }
      }
      //get data from server
      fetchData();

      // In development mode the background.js scripts does not run
      // so we need to save the user on the client side in order to make the app to function properly
      if (
        process.env.NODE_ENV === "development" &&
        (localStorage.getItem("userId") === null ||
          localStorage.getItem("userId") === undefined)
      ) {
        const data = {
          token: Date.now(),
          language: i18n.language.split("-")[0],
          browser_name: "Chrome",
        };
        //register this user
        fetch(`${Server["BASE_URl"]}/extension/add-user`, {
          method: "POST",
          cache: "no-cache",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        })
          .then((response) => {
            if (response.ok) {
              localStorage.setItem("userId", JSON.stringify(data));
            }
            console.log(response);
          })
          .catch((err) => console.log(err.message));
      }
    }
  }, [list, setList]);

  return (
    <div className="App">
      {list === undefined ? (
        <Preloader label={t("In caricamento...")} />
      ) : (
        <div className="app-body">
          <AppContextProvider>
            <Header setPage={toggleView} translate={t} />
            <Suspense
              fallback={<Preloader label={t("Componente in caricamento...")} />}
            >
              {page === "search" || page === "mylists" ? (
                <SearchContextProvider>
                  {page === "search" ? <Search data={list} /> : <MyLists />}
                </SearchContextProvider>
              ) : (
                <Inbox />
              )}
            </Suspense>
          </AppContextProvider>
        </div>
      )}
    </div>
  );
};

export default App;
