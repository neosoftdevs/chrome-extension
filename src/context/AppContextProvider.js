import React, { useReducer, createContext } from "react";

export const AppContext = createContext();

const initialState = {
  lists: [],
  inbox: 0,
};

const reducer = (state, action) => {
  switch (action.type) {
    case "ADD_LIST":
      return {
        lists: [...state.lists, action.payload],
        inbox: state.inbox,
      };
    case "REMOVE_LIST":
      return {
        lists: state.lists.filter((item) => item.pi !== action.payload),
        inbox: state.inbox,
      };
    case "UPDATE_INBOX":
      return {
        lists: [...state.lists],
        inbox: action.payload,
      };
    case "UPDATE_LIST":
      return {
        lists: state.lists.map((item) => {
          if (item.pi === action.payload.id) {
            if (typeof action.payload.status !== "undefined") {
              item.updates = action.payload.status;
            }
            if (typeof action.payload.discount !== "undefined") {
              if (
                action.payload.discount === "" ||
                action.payload.discount === null ||
                action.payload.discount === 0
              ) {
                if (typeof item.updatesInfo !== "undefined") {
                  delete item.updatesInfo;
                }
                item.d = null;
              } else {
                if (typeof item.updatesInfo === "undefined") {
                  item.d = action.payload.discount;
                } else {
                  if (action.payload.discount < item.updatesInfo.discount) {
                    item.updates = true;
                    item.d = action.payload.discount;
                    delete item.updatesInfo;
                  } else {
                    item.updatesInfo.discount = action.payload.discount;
                  }
                }
              }
            }
          }
          return item;
        }),
        inbox: state.inbox,
      };
    default:
      throw new Error();
  }
};

export const AppContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={[state, dispatch]}>
      {props.children}
    </AppContext.Provider>
  );
};
