# Getting Started with Bancomail Chrome Extension

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

then

### `npm start`

or

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\

After building copy the file from static/css and paste to css/ folder, then rename the file to "styles.css"

Copy all .js files from static/js and paste them to js/ folder

Open index.html file, cut the inline js script and paste to js/initialize.js file, then load the initialize.js into index.html via the "script" tag

In index.html check for the correct src path (es. "/js/filename.js") of all other scripts

Delete "static" folder.


Open Google Chrome browser

paste "chrome://extensions" into the url bar

check the "Developer mode btn"

then click Load unpacked btn -> load the build folder

