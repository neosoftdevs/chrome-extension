/*global chrome*/
const BASE_URL = "http://bancoservice.bancolab.com";

document.addEventListener(
  "DOMContentLoaded",
  () => {
    document.getElementById("selectorLabel").textContent =
      chrome.i18n.getMessage("languageSelector");
    document.getElementById("save").textContent =
      chrome.i18n.getMessage("saveOptionsBtn");
    chrome.storage.local.get(["bm_lang"], function (result) {
      if (result.bm_lang) {
        document.querySelectorAll('input[name="language"]').forEach((item) => {
          if (item.value === result.bm_lang) {
            item.checked = true;
          }
        });
      }
    });
  },
  false
);

document.getElementById("save").addEventListener(
  "click",
  (e) => {
    document.querySelectorAll('input[name="language"]').forEach((item) => {
      if (item.checked === true) {
        chrome.storage.local.set({ bm_lang: item.value }, function () {
          var status = document.getElementById("status");
          status.textContent = chrome.i18n.getMessage("saveOptionStatus");
          setTimeout(function () {
            status.textContent = "";
          }, 750);
          chrome.storage.local.get(["bm_token"], function (result) {
            if (result.bm_token) {
              fetch(`${BASE_URL}/extension/update-user`, {
                method: "PATCH",
                cache: "no-cache",
                mode: "cors",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  token: `${result.bm_token}`,
                  language: item.value,
                }),
              })
                .then((getResponse) => getResponse.json())
                .then((response) => console.log(response.msg))
                .catch((err) => console.log(err.message));
            }
          });
          return;
        });
      }
    });
  },
  false
);
