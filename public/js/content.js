/*global chrome*/
var s = document.createElement("script");
s.src = chrome.runtime.getURL("js/script.js");
(document.head || document.documentElement).appendChild(s);
s.onload = function () {
  s.remove();
};

// Events listeners

//Register extension user - associate bancomail_user_id to a extension token
document.addEventListener("BANCOMAIL_registerUser", function (e) {
  const loginForm = document.querySelector("#logInRequest");
  const loginPage = document.querySelector("#login-page");
  const formReg = document.querySelector("#formreg");
  if (loginForm !== null) {
    chrome.storage.local.get(["bm_token"], function (result) {
      if (result.bm_token) {
        const inputField = document.createElement("input");
        inputField.name = "extension-token";
        inputField.value = result.bm_token;
        inputField.type = "hidden";
        loginForm.insertBefore(inputField, loginForm.children[0]);
        if (loginPage !== null) {
          loginPage.insertBefore(inputField, loginPage.children[0]);
        }
        if (formReg !== null) {
          formReg.insertBefore(inputField, formReg.children[0]);
        }
      }
    });
  }
});

// Inject a html btn to monitor a list:
document.addEventListener("BANCOMAIL_connectExtension", async function (e) {
  // e.detail contains the transferred data (can be anything, ranging
  // from JavaScript objects to strings).
  chrome.storage.local.get(["bm_my_lists"], async function (result) {
    const previousSavedLists = result.bm_my_lists
      ? JSON.parse(result.bm_my_lists)
      : [];
    const container = document.querySelectorAll(".email-pack");
    if (container !== null) {
      const getLanguage = () =>
        new Promise((resolve, reject) => {
          chrome.storage.local.get(["bm_lang"], function (result) {
            if (result.bm_lang) {
              resolve(result.bm_lang);
            } else {
              reject(false);
            }
          });
        });

      const language = await getLanguage();

      container.forEach((item) => {
        let list = JSON.parse(item.dataset.pack);
        if (list.sconto >= 50) {
          return;
        }
        let savedList = previousSavedLists.filter(
          (element) => element.pi === list.id
        );
        let btn = document.createElement("span");
        let icn = document.createElement("i");
        if (savedList.length > 0 && savedList[0].updates === true) {
          icn.className = "icon-eye-open tip text-grey";
        } else {
          icn.className = "icon-eye-open tip text-light";
        }
        btn.appendChild(icn);
        if (savedList.length > 0 && savedList[0].updates === true) {
          btn.setAttribute("title", chrome.i18n.getMessage("bmSyncBtnLabel2"));
        } else {
          btn.setAttribute("title", chrome.i18n.getMessage("bmSyncBtnLabel"));
        }
        btn.style = "cursor:pointer;position:absolute;top:20px;right:5px";

        btn.addEventListener(
          "click",
          (e) => {
            e.preventDefault();
            let target =
              e.target.tagName === "SPAN" ? e.target.childNode : e.target;
            try {
              target.classList.toggle("text-grey");
              if (target.classList.contains("text-grey")) {
                target.parentNode.setAttribute(
                  "title",
                  chrome.i18n.getMessage("bmSyncBtnLabel2")
                );
                target.classList.remove("text-light");
              } else {
                target.parentNode.setAttribute(
                  "title",
                  chrome.i18n.getMessage("bmSyncBtnLabel")
                );
                target.classList.add("text-light");
              }
            } catch (e) {
              //console.log(e.messsge);
              return;
            }

            chrome.storage.local.get(["bm_my_lists"], async function (result) {
              const myLists = result.bm_my_lists
                ? JSON.parse(result.bm_my_lists)
                : [];
              //if the list was added or was removed
              let getMyLists = target.classList.contains("text-grey")
                ? myLists.filter((item) => item.pi === list.id)
                : myLists.filter((item) => item.pi !== list.id);

              if (
                getMyLists.length === 0 &&
                target.classList.contains("text-grey") === true
              ) {
                //we have a new list

                let pack = "";
                const endPoint = window.location.href.includes(".it")
                  ? `https://www.bancomail.it/listino-email-completo/get-pac-info?pid=${list.id}&lg=${language}`
                  : `https://www.bancomail.com/en/email-directory/get-pac-info?pid=${list.id}&lg=${language}`;
                try {
                  let response = await fetch(endPoint);
                  let info = await response.json();
                  pack = info.data[0];
                } catch (e) {
                  //console.log(e.message);
                  target.classList.remove("text-grey");
                  target.classList.add("text-light");
                  return;
                }

                myLists.push({
                  //replace object with fetch data
                  pi: pack.pi,
                  pn: pack.pn,
                  iso: pack.iso,
                  n: pack.n,
                  re: pack.re === null ? "Tutte le regioni" : pack.re,
                  d: pack.d,
                  p: pack.p,
                  i: pack.i,
                  updates: true,
                  watch: Date.now(),
                });
              }

              const listToSave = target.classList.contains("text-grey")
                ? JSON.stringify(myLists)
                : JSON.stringify(getMyLists);

              chrome.storage.local.set(
                { bm_my_lists: listToSave },
                function () {
                  return;
                }
              );
            });
          },
          false
        );

        item.lastElementChild.appendChild(btn);
      });
    }
  });
});
