/*global chrome*/
const BASE_URL = "http://bancoservice.bancolab.com";
let usrToken = "";
function showBadge(ids) {
  try {
    chrome.action.setBadgeBackgroundColor({ color: "#25baa5" }, () => {
      chrome.action.setBadgeText({ text: `${ids.length}` }, () => false);
    });
  } catch (e) {
    console.log(e.message);
  }
}

const htmlDecode = (txt) => {
  if (txt === "" || typeof txt === "undefined" || txt === null) {
    return;
  }
  let replaceChars = {
    "&lt;": "<",
    "&gt;": ">",
    "&apos;": "'",
    "&quot;": '"',
    "&amp;": "&",
  };
  let decodedTxt = txt.replace(
    /&lt;|&gt;|&apos;|&quot;|&amp;/g,
    function (match) {
      return replaceChars[match];
    }
  );
  return decodedTxt;
};

const removeSlashes = (string) => {
  if (typeof string !== "undefined" && string.length > 0) {
    string = string
      .replace(/\\"/g, '"')
      .replace(/\\'/g, "'")
      .replace(/\\r/g, "\r")
      .replace(/\\f/g, "\f")
      .replace(/\\n/g, "\n")
      .replace(/\\t/g, "\t")
      .replace(/\\b/g, "\u0008")
      .replace(/\\\\/g, "\\");
    return string.trim();
  }
};

const getLanguage = () =>
  new Promise((resolve, reject) => {
    chrome.storage.local.get(["bm_lang"], function (result) {
      if (result.bm_lang) {
        resolve(result.bm_lang);
      } else {
        reject(false);
      }
    });
  });

const getToken = () =>
  new Promise((resolve, reject) => {
    chrome.storage.local.get(["bm_token"], function (result) {
      if (result.bm_token) {
        usrToken = result.bm_token;
        resolve(result.bm_token);
      } else {
        reject(false);
      }
    });
  });

const getNotifications = () =>
  new Promise((resolve, reject) => {
    chrome.storage.local.get(["bm_notifications"], function (result) {
      if (result.bm_notifications) {
        resolve(JSON.parse(result.bm_notifications));
      } else {
        reject(false);
      }
    });
  });

//run this script when user install the extension
chrome.runtime.onInstalled.addListener(() => {
  const token = Date.now();
  const language = navigator.language.split("-")[0];
  chrome.storage.local.set({ bm_lang: language }, function () {
    return;
  });
  chrome.storage.local.set({ bm_my_lists: JSON.stringify([]) }, function () {
    return;
  });
  chrome.storage.local.set({ bm_token: token }, function () {
    data = {
      token: token,
      language: language,
      browser_name: "Chrome",
    };
    //register this user
    fetch(`${BASE_URL}/extension/add-user`, {
      method: "POST",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        if (response.ok) {
          console.log("The user was registred successfully!");
        }
      })
      .catch((err) => console.log(err.message));
    return;
  });

  chrome.storage.local.set(
    { bm_notifications: JSON.stringify([]) },
    function () {
      return;
    }
  );
});

//run every time when the storage is caaled
chrome.storage.onChanged.addListener((changes, area) => {
  if (area === "local" && changes.bm_my_lists?.newValue) {
    watchMyLists();
  }
});

chrome.runtime.onStartup.addListener(() => {
  //console.log('onStartup....');
  //start connection
  watchMyLists();
});

chrome.runtime.onSuspend.addListener(() => {
  //console.log("Unloading.");
  watchMyLists();
});

function translate(key, lg) {
  const language = navigator.language || navigator.userLanguage;
  let lang = !lg ? language.split("-")[0] : lg;
  const obj = {
    bmMsgTitle: {
      it: "Notifica da Bancomail",
      en: "Notification from Bancomail",
      es: "Notificación por Bancomail",
      fr: "Notification par Bancomail",
      de: "Bancomail-Meldung",
    },
    bmMsgDescr: {
      it: "Uno o più dei Database che osservi sono scontati!",
      en: "Special Sale on some of the databases you are watching! Check now!",
      es: "¡Una o varias de las bases de datos que observas están descontadas!",
      fr: "¡Una o varias de las bases de datos que observas están descontadas!",
      de: "Eine oder mehrere der von Ihnen beobachteten Datenbanken sind rabattiert!",
    },
    bmNotificationDescr: {
      it: "Hai nuovi messagi non letti!",
      en: "You have new messages in yout inbox",
      es: "",
      fr: "",
      de: "",
    },
    bmMsgBtnLabel: {
      it: "Scopri ora!",
      en: "Find out more!",
      es: "¡Descúbrelo ahora!",
      fr: "Vérifiez",
      de: "Entdecken Sie es jetzt!",
    },
    bmNotificationBtnLabel: {
      it: "Leggi ora!",
      en: "Read now!",
      es: "¡Descúbrelo ahora!",
      fr: "Vérifiez",
      de: "Entdecken Sie es jetzt!",
    },
    controller: {
      it: "liste-email",
      en: "email-database",
      es: "bases-de-datos-de-emails",
      fr: "fichier-email",
      de: "e-mail-datenbank",
    },
    action: {
      it: "carrello",
      en: "shopping-cart",
      es: "cesta-compra",
      fr: "panier",
      de: "einkaufswagen",
    },
  };
  return obj[key][lang];
}

function watchMyLists() {
  chrome.storage.local.get(["bm_my_lists"], async function (result) {
    if (result.bm_my_lists) {
      let getLists = JSON.parse(result.bm_my_lists);
      const watchLists = getLists.filter((item) => item.updates === true);

      if (watchLists.length > 0) {
        const dataToSend = [];
        watchLists.forEach((item) => {
          dataToSend.push(item.pi);
        });

        const token = await getToken();

        const timestamp = Date.now();

        //console.log('Connect to server: '+timestamp);

        fetch(
          `${BASE_URL}/extension/get-updates?items=${encodeURIComponent(
            JSON.stringify(dataToSend)
          )}&token=${token}`
        )
          .then((response) => response.json())
          .then(async (response) => {
            const getData = response;

            //console.log(getData);

            if (getData.ok === false) {
              return;
            }

            const updates = [];
            getData.data.forEach((item) => {
              watchLists.forEach((element) => {
                if (element.pi === item.pi && element.d !== item.d) {
                  let key = element;
                  key["updatesInfo"] = { discount: item.d };
                  updates.push(key);
                }
              });
            });
            if (updates.length === 0) {
              //console.log('No updates!');
              return;
            }

            const ids = [];

            updates.forEach((item) => {
              getLists.forEach((element) => {
                if (item.pi === element.pi && item.updatesInfo) {
                  if (element.d === null) {
                    if (element.d !== item.updatesInfo.discount) {
                      element.updates = false;
                      element.updatesInfo = item.updatesInfo;
                      ids.push(item.pi);
                    }
                  } else {
                    if (
                      item.updatesInfo.discount === null ||
                      item.updatesInfo.discount === 0 ||
                      item.updatesInfo.discount === ""
                    ) {
                      element.d = item.updatesInfo.discount;
                    } else {
                      if (element.d < item.updatesInfo.discount) {
                        element.updates = false;
                        element.updatesInfo = item.updatesInfo;
                        ids.push(item.pi);
                      } else {
                        element.d = item.updatesInfo.discount;
                      }
                    }
                  }
                }
              });
            });

            const language = await getLanguage();
            ////console.log(getLists);
            chrome.storage.local.set(
              { bm_my_lists: JSON.stringify(getLists) },
              function () {
                if (ids.length > 0) {
                  chrome.notifications.create("bm_test", {
                    type: "basic",
                    iconUrl: "/icons/launcher-icon-2x.png",
                    title: translate("bmMsgTitle", language),
                    message: translate("bmMsgDescr", language),
                    buttons: [
                      {
                        title: translate("bmMsgBtnLabel", language),
                      },
                    ],
                    priority: 2,
                  });
                  /* Respond to the user's clicking one of the buttons */
                  chrome.notifications.onButtonClicked.addListener(function (
                    notifId,
                    btnIdx
                  ) {
                    if (notifId === "bm_test") {
                      const path =
                        language === "it"
                          ? `https:www.bancomail.it/liste-email/carrello?packs=${ids.join(
                              ","
                            )}`
                          : `https:www.bancomail.com/${language}/${translate(
                              "controller",
                              language
                            )}/${translate(
                              "action",
                              language
                            )}?packs=${ids.join(",")}`;
                      //window.open(path, "_blank");
                      chrome.tabs.create({ url: path, active: true });
                      //window.open("index.html", "extension_popup", "width=550,height=520,status=no,scrollbars=none,resizable=no");
                    }
                  });
                } //end if ids.length > 0

                /* Add this to also handle the user's clicking
                 * the small 'x' on the top right corner */
                /*chrome.notifications.onClosed.addListener(function() {
                        
                    });*/
              }
            );

            //update extension badge
            if (ids.length > 0) {
              showBadge(ids);
            }
          })
          .catch((error) => {
            console.log(error.message);
          });
        getFreshInfo(1);
      } else {
        //console.log('No lists to watch 4!');
        getFreshInfo(2);
      }
    } else {
      //console.log('No saved lists!');
      getFreshInfo(1);
    }
  });
}

function getFreshInfo(monitoring_packs) {
  /*
  monitoring_packs = 1 bulk message | 2 bulk with no monitoring packs
  */
  getToken()
    .then(async (token) => {
      const language = await getLanguage();
      fetch(
        `${BASE_URL}/extension/get-fresh-notifications?token=${token}&lang=${language}&bulk=${monitoring_packs}`
      )
        .then((getData) => getData.json())
        .then(async (response) => {
          const oldNotifications = await getNotifications();

          if (response.ok && response.data !== undefined) {
            const newNotifications = [];

            response.data.forEach((item) => {
              let status = true;
              oldNotifications.forEach((element) => {
                if (
                  element.start_date === item.start_date &&
                  element.end_date === item.end_date &&
                  element.bulk === item.bulk &&
                  element.lang === item.lang
                ) {
                  status = false;
                }
                if (
                  item.user_token !== undefined &&
                  item.user_token !== null &&
                  item.user_token !== "" &&
                  item.user_token === element.user_token
                ) {
                  status = false;
                }
              });
              if (status) {
                item["received_date"] = Date.now();
                newNotifications.push(item);
              }
            });

            //delete old canceled notifications
            const d = new Date();
            const today = `${d.getFullYear()}-${d.getMonth()}-${d.getDay()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
            const deleteAction = false;
            oldNotifications.forEach((item) => {
              if (
                new Date(item.end_date) < new Date(today) &&
                item.message_title === undefined &&
                item.message_descr === undefined
              ) {
                delete item;
                deleteAction = true;
              }
            });

            if (newNotifications.length === 0) {
              //save in storage and get out
              if (deleteAction) {
                chrome.storage.local.set(
                  { bm_notifications: JSON.stringify(oldNotifications) },
                  function () {
                    return;
                  }
                );
              }
              return;
            }

            //mark new notification as old
            newNotifications.forEach((item) => oldNotifications.unshift(item));

            //update old notifications
            chrome.storage.local.set(
              { bm_notifications: JSON.stringify(oldNotifications) },
              function () {
                //send system notification to the user
                chrome.notifications.create("bm_info", {
                  type: "basic",
                  iconUrl: "/icons/launcher-icon-2x.png",
                  title:
                    newNotifications.length > 1
                      ? translate("bmMsgTitle", language)
                      : htmlDecode(
                          removeSlashes(newNotifications[0].message_title)
                        ),
                  message:
                    newNotifications.length > 1
                      ? translate("bmNotificationDescr", language)
                      : htmlDecode(newNotifications[0].message_descr),
                  buttons: [
                    {
                      title:
                        newNotifications.length > 1
                          ? translate("bmNotificationBtnLabel", language)
                          : htmlDecode(
                              removeSlashes(newNotifications[0].btn_title)
                            ),
                    },
                  ],
                  priority: 2,
                });
                //manage notification click btn
                chrome.notifications.onButtonClicked.addListener(function (
                  notifId,
                  btnIdx
                ) {
                  if (notifId === "bm_info") {
                    //if we have a single notification
                    if (newNotifications.length === 1) {
                      chrome.tabs.create({
                        url: removeSlashes(newNotifications[0].call_to_action),
                        active: true,
                      });
                    } else {
                      window.open(
                        "index.html",
                        "extension_popup",
                        "width=550,height=520,status=no,scrollbars=none,resizable=no"
                      );
                    }
                  }
                });
                showBadge(newNotifications);
              }
            );
          } else {
            //we don't have any kind of notifications so
            //delete old canceled notifications
            const d = new Date();
            const today = `${d.getFullYear()}-${d.getMonth()}-${d.getDay()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
            let deleteAction = false;
            oldNotifications.forEach((item) => {
              if (
                new Date(item.end_date) < new Date(today) &&
                item.message_title === undefined &&
                item.message_descr === undefined
              ) {
                delete item;
                deleteAction = true;
              }
            });
            //save in storage
            if (deleteAction) {
              chrome.storage.local.set(
                { bm_notifications: JSON.stringify(oldNotifications) },
                function () {
                  return;
                }
              );
            }
          }
        });
      //save a timestamp to track user activìty
      trackUser(token);
    })
    .catch((err) => console.log(err.message));
}

async function trackUser(token) {
  if (!token) {
    console.log("Token is undefined!");
    return;
  }
  fetch(`${BASE_URL}/extension/update-user`, {
    method: "PATCH",
    cache: "no-cache",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      token: `${token}`,
      language: await getLanguage(),
    }),
  })
    .then((getResponse) => getResponse.json())
    .then((response) => console.log(response.msg))
    .catch((err) => console.log(err.message));
}

watchMyLists();

//wake-up service worker every 1 minute and perform a server connection
chrome.alarms.create({ periodInMinutes: 1 });
chrome.alarms.onAlarm.addListener(() => {
  //console.log('Alarm start');
  watchMyLists();
});
