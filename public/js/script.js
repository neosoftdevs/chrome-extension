setTimeout(function () {
  /* Example: Send data from the page to your Chrome extension */
  const container = document.querySelectorAll(".email-pack");
  const login = document.querySelector("a.login-btn i");

  //user is not logged on and login form is available
  if (login !== null && login.classList.contains("icon-lock")) {
    //inject a hidden field with token
    document.dispatchEvent(
      new CustomEvent("BANCOMAIL_registerUser", {
        detail: window.location, // Some variable
      })
    );
  }

  //trigger this event in the email-databse page
  if (container !== null) {
    document.dispatchEvent(
      new CustomEvent("BANCOMAIL_connectExtension", {
        detail: window.location, // Some variable
      })
    );
  }
}, 0);
